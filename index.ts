import dotenv from 'dotenv';

import bugsnag from '@bugsnag/js';
import bugsnagExpress from '@bugsnag/plugin-express';
import express from 'express';
import next from 'next';

// Routes
import appRoutes from './api/routes/app.routes';

dotenv.config();
export const dev = process.env.NODE_ENV !== 'production';
export const bugsnagClient = bugsnag({
  apiKey: process.env.BUGSNAG_API_KEY,
  releaseStage: process.env.NODE_ENV,
  notifyReleaseStages: [
    // 'development',
    'staging',
    'production',
  ],
});

bugsnagClient.use(bugsnagExpress);

const app = next({ dev });
const handle = app.getRequestHandler();
const middleware = bugsnagClient.getPlugin('express');

const init = async () => {
  try {
    await app.prepare();
    const server = express();
    server.use(middleware.requestHandler);

    server.use('/api/v1/app', appRoutes);

    server.get('/gift/:id', (req, res) => {
      const actualPage = '/post';
      const queryParams = { id: req.params.id };
      app.render(req, res, actualPage, queryParams);
    });

    server.get('*', (req, res) => {
      return handle(req, res);
    });

    server.listen(3000, (err: string) => {
      if (err) {
        throw err;
      }
      // tslint:disable-next-line
      console.log('> Ready on http://localhost:3000');
    });

    server.use(middleware.errorHandler);
  } catch (ex) {
    // tslint:disable-next-line
    console.error(ex.stack);
    process.exit(1);
  }
};

init();

import React, { Component } from 'react';
import Header from '../components/Header';
import GlobalReset from '../styled/global';

class Index extends Component {
  public static async getInitialProps() {
    return {};
  }

  public render() {
    return (
      <div>
        <GlobalReset />
        <Header />
        <div>
          <div>Sidebar</div>
          <div>Content</div>
        </div>
      </div>
    );
  }
}

export default Index;

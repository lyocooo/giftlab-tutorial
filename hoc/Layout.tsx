import React from 'react';
import Header from '../components/Header';

const layoutStyle = {
  margin: 20,
  border: '1px solid #DDD',
  padding: 20,
};
interface IProps {
  children: JSX.Element | JSX.Element[];
}

const Layout = (props: IProps) => (
  <div style={layoutStyle}>
    <Header />
    {props.children}
  </div>
);

export default Layout;

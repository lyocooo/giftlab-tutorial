import { Router } from 'express';
import AppController from '../controllers/app.controller';

const router = Router();

router.route('/').get(AppController.getAppStatus);

export default router;

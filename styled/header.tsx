import styled from 'styled-components';

const HeaderContainer = styled.div`
  min-height: 480px;
  background-color: green;
  background-image: url(https://upload.wikimedia.org/wikipedia/commons/0/0c/Arabian_Sea_-_n22e70.jpg);
  background-size: 100%;
  background-position: center;
`;

const MenuContainer = styled.div`
  display: flex;
  padding: 15px 50px;
  align-items: center;
`;

const LogoImg = styled.img`
  margin-right: 80px;
`;

const InternalMenuContainer = styled.div`
  display: flex;

  margin-right: 20px;
`;

const InternalMenuElement = styled.a`
  margin-right: 50px;
`;

const SearchContainer = styled.div`
  background: rgba(255, 255, 255, 0.3);
  border-radius: 12px;
  height: 50px;
  flex: 1;
  display: flex;
`;

const SearchIconContainer = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SearchInput = styled.input`
  flex: 1;
  background: transparent;
  padding-right: 15px;
`;

export {
  HeaderContainer,
  MenuContainer,
  LogoImg,
  InternalMenuContainer,
  InternalMenuElement,
  SearchContainer,
  SearchIconContainer,
  SearchInput,
};

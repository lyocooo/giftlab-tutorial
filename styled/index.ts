import styled from 'styled-components';

interface IProps {
  flex?: number;
  width?: number;
  height?: number;
  display?: string;
}

const Layout = styled.div`
  ${(props: IProps): string => (props.flex ? `flex: ${props.flex};` : '')}
  ${(props: IProps): string => (props.width ? `width: ${props.width}px;` : '')}
  ${(props: IProps): string =>
    props.height ? `height: ${props.height}px;` : ''}
  ${(props: IProps): string =>
    props.display ? `display: ${props.display};` : ''}
`;

const P = styled.p``;

export { Layout };
export default P;
